import codecs
import os
import shutil
import requests
import urllib
import urllib.request


def readFile(params):
    path = params['path']
    cont = ''
    code = params['code']
    try:
        with codecs.open(path, 'r', code) as file:
            # line = file.readline()
            data = file.readlines()
            for line in data:
                cont = cont + line
    except:
        raise Exception("读取文件出错")
    return cont


def writeFile(params):
    path = params['path']
    code = params['code']
    try:
        with codecs.open(path, 'w', code) as file:
            file.write(str(params['content']))
    except:
        raise Exception(message='写入文件出错')
    return


def addWriteFile(params):
    path = params['path']
    code = params['code']
    try:
        with codecs.open(path, 'a', code) as file:
            file.write(str(params['content']))
    except:
        raise Exception(message='追加写入文件出错')
    return


def getSize(params):
    path = params['path']
    try:
        size = os.path.getsize(path)
    except Exception as err:
        raise Exception(message='读取文件出错')
    return size


def copyFile(params):
    path = params['path']
    tarDir = params['tarPath']
    cover = params['cover']
    *_, filename = os.path.split(path)
    target_path = '%s\\%s' % (tarDir, filename)
    try:
        if 'yes' == cover:
            if os.path.exists(target_path):
                shutil.copyfile(path, '%s\\%s.tempfile' % (tarDir, filename))
                os.remove(target_path)
                os.rename('%s\\%s.tempfile' % (tarDir, filename), target_path)
            else:
                shutil.copy(path, tarDir)
        elif 'no' == cover:
            if os.path.exists(target_path):
                shutil.copyfile(path, '%s\\副本_%s' % (tarDir, filename))
            else:
                shutil.copy(path, tarDir)
    except Exception as e:
        print(e)
        raise e# Exception("复制文件出错")
    return


def moveFile(params):
    path = params['path']
    tarDir = params['tarPath']
    cover = params['cover']
    *_, filename = os.path.split(path)
    params['path'] = tarDir
    try:
        if 'no' == cover:
            for file in list_files(params):
                if file == tarDir + "\\" + filename:
                    break
                else:
                    shutil.move(path, tarDir)
        elif 'yes' == cover:
            for file in list_files(params):
                if file == tarDir + "\\" + filename:
                    params['path'] = file
                    delFile(params)
                    shutil.move(path, tarDir)
                    break
                else:
                    shutil.move(path, tarDir)
    except:
        raise Exception(message="移动文件出错")
    return


def newName(params):
    path = params['path']
    new_name = params['newName']
    ext = os.path.splitext(path)[1]
    try:
        filePath, old_name = os.path.split(path)
        os.rename(path, filePath + "\\" + new_name + ext)
    except Exception as err:
        raise Exception("重命名文件出错")
    return


def delFile(params):
    path = params['path']
    try:
        os.remove(path)
    except:
        raise Exception("删除文件出错")
    return


def createDir(params):
    path = params['path']
    dirName = params['name']
    try:
        os.mkdir(path + "\\" + dirName)
    except:
        raise Exception("创建文件夹出错")
    return


def createFile(params):
    path = params['path']
    fileName = params['name']
    try:
        with open(path + "\\" + fileName, 'w+')as file:
            pass
    except:
        raise Exception('创建文件出错')


def fileIsExist(params):
    path = params['path']
    data = ''
    try:
        if os.path.exists(path):
            data = '文件存在'
        else:
            data = '文件不存在'
    except:
        raise Exception('判断出错')
    return data


def pathIsExist(params):
    path = params['path']
    data = ''
    try:
        if os.path.exists(path):
            data = '路径存在'
        else:
            data = '路径不存在'
    except:
        raise Exception("判断路径出错")
    return data


def list_files(params):
    path = params['path']
    result = []
    try:
        for file in os.listdir(path):
            if os.path.isfile(path + '\\' + file):
                result.append(path + "\\" + file)
    except:
        raise Exception('获取文件列表出错')
    return result


def list_dirs(params):
    path = params['path']
    result = []
    try:
        for dir in os.listdir(path):
            if os.path.isdir(path + '\\' + dir):
                result.append(path + "\\" + dir)
    except:
        raise Exception('获取文件夹列表出错')
    return result


def findFile(params):
    path = params['path']
    fileName = params['name']
    deep = params['deep']
    result = ''
    try:
        if 'yes' == deep:
            for mainpath, dirs, files in os.walk(path):
                if fileName in files:
                    result = os.path.join(mainpath, fileName)
                    break
                else:
                    result = "文件不存在"
        elif 'no' == deep:
            for file in list_files(params):
                filepath, file_name = os.path.split(file)
                if file_name == fileName:
                    result = path + "\\" + fileName
                    break
                else:
                    result = "文件不存在"
    except:
        raise Exception("查找文件出错")
    return result


# p = {"path": r"C:\Users\Administrator\Desktop\新建文本文档.txt",
#      "tarPath": r"F:\BaofengVideo",
#      "content": "sfhdhdzd",
#      'code': 'gbk',
#      "newName": "哈哈",
#      "name": "各种密码.txt",
#      "deep": "yes",
#      "cover": "yes"}
#
# print(readFile(p))
#
# newName(p)

def imageToPdf(params):
    try:
        current_dir = os.path.dirname(os.path.abspath(__file__))
        pdf_file_path = params['pdf_path'] + "\\" + params['pdf_name']
        os.system("%s\\convert.exe %s\\* %s" % (current_dir, params['image_path'], pdf_file_path))
        return pdf_file_path
    except Exception as e:
        print(e)
        raise e


def saveImageFromUrl(params):
    try:
        if 'download_url' not in params.keys() or params['download_url'] is None or params['download_url'] == '':
            raise Exception('缺少参数：图片下载链接')
        elif 'image_path' not in params.keys() or params['image_path'] is None or params['image_path'] == '':
            raise Exception('缺少参数：pdf保存目录')
        elif 'image_name' not in params.keys() or params['image_name'] is None or params['image_name'] == '':
            raise Exception('缺少参数：图片保存名称')
        else:
            request = urllib.request.Request(params["download_url"])
            response = urllib.request.urlopen(request)
            get_img = response.read()
            with open("%s\\%s" % (params["image_path"], params["image_name"]), "wb") as fp:
                fp.write(get_img)

            return "%s\\%s" % (params["image_path"], params["image_name"])
    except Exception as e:
        raise e


def saveFileFromUrl(params):
    try:
        if 'download_url' not in params.keys() or params['download_url'] is None or params['download_url'] == '':
            raise Exception('缺少参数：图片下载链接')
        elif 'save_path' not in params.keys() or params['save_path'] is None or params['save_path'] == '':
            raise Exception('缺少参数：保存目录')
        elif 'file_name' not in params.keys() or params['file_name'] is None or params['file_name'] == '':
            raise Exception('缺少参数：保存名称')
        else:
            file_path = "%s\\%s" % (params['save_path'], params['file_name'])
            if params['request_method'] == 'post':
                res = requests.get(params['download_url'], data=params['request_params'], headers=params['request_headers'])
            elif params['request_method'] == 'get':
                res = requests.get(params['download_url'], data=params['request_params'], headers=params['request_headers'])
            else:
                res = None
            
            if res is not None:
                with open(file_path, "wb") as save_file:
                    for chunk in res.iter_content(chunk_size=512):
                        if chunk:
                            save_file.write(chunk)
            else:
                raise Exception("请求失败")
            
        return file_path
    except Exception as e:
        raise e


def getFileName(params):
    if 'file_path' not in params.keys() or params['file_path'] is None or params['file_path'] == '':
        raise Exception('缺少参数：文件路径')
    else:
        basename = os.path.basename(params["file_path"])
        return basename.split(".")[0]



if __name__ == "__main__":
    pass
