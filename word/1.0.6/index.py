import win32com
import win32api
import wmi
import os
import sys
from docx import Document
import re

oWord = None

version = ''

# WdOpenFormat
# A Microsoft Word format that is backward compatible with earlier versions of Word.
wdOpenFormatAllWord = 6
wdOpenFormatAuto = 0  # The existing format.
wdOpenFormatDocument = 1  # Word format.
wdOpenFormatEncodedText = 5  # Encoded text format.
wdOpenFormatRTF = 3  # Rich text format (RTF).
wdOpenFormatTemplate = 2  # As a Word template.
wdOpenFormatText = 4  # Unencoded text format.
wdOpenFormatOpenDocumentText = 18  # (&H12)	OpenDocument Text format.
wdOpenFormatUnicodeText = 5  # Unicode text format.
wdOpenFormatWebPages = 7  # HTML format.
wdOpenFormatXML = 8  # XML format.
wdOpenFormatAllWordTemplates = 13  # Word template format.
wdOpenFormatDocument97 = 1  # Microsoft Word 97 document format.
wdOpenFormatTemplate97 = 2  # Word 97 template format.
wdOpenFormatXMLDocument = 9  # XML document format.
# Open XML file format saved as a single XML file.
wdOpenFormatXMLDocumentSerialized = 14
# XML document format with macros enabled.
wdOpenFormatXMLDocumentMacroEnabled = 10
# Open XML file format with macros enabled saved as a single XML file.
wdOpenFormatXMLDocumentMacroEnabledSerialized = 15
wdOpenFormatXMLTemplate = 11  # XML template format.
# (&H10)	Open XML template format saved as a XML single file.
wdOpenFormatXMLTemplateSerialized = 16
# XML template format with macros enabled.
wdOpenFormatXMLTemplateMacroEnabled = 12
# (&H11)	Open XML template format with macros enabled saved as a single XML file.
wdOpenFormatXMLTemplateMacroEnabledSerialized = 17

# WdCollapseDirection
wdCollapseEnd = 0  # Collapse the range to the ending point.
wdCollapseStart = 1  # Collapse the range to the starting point.

# WdUnits
wdCell = 12  # A cell.
wdCharacter = 1  # A character.
wdCharacterFormatting = 13  # Character formatting.
wdColumn = 9  # A column.
wdItem = 16  # The selected item.
wdLine = 5  # A line.
wdParagraph = 4  # A paragraph.
wdParagraphFormatting = 14  # Paragraph formatting.
wdRow = 10  # A row.
wdScreen = 7  # The screen dimensions.
wdSection = 8  # A section.
wdSentence = 3  # A sentence.
wdStory = 6  # A story.
wdTable = 15  # A table.
wdWindow = 11  # A window.
wdWord = 2  # A word.

# WdFindWrap
# After searching the selection or range, Microsoft Word displays a message asking whether to search the remainder of the document.
wdFindAsk = 2
# The find operation continues if the beginning or end of the search range is reached.
wdFindContinue = 1
# The find operation ends if the beginning or end of the search range is reached.
wdFindStop = 0

# WdReplace
wdReplaceAll = 2  # Replace all occurrences.
wdReplaceNone = 0  # Replace no occurrences.
wdReplaceOne = 1  # Replace the first occurrence encountered.

# WdGoToItem
wdGoToBookmark = -1  # A bookmark.
wdGoToComment = 6  # A comment.
wdGoToEndnote = 5  # An endnote.
wdGoToEquation = 10  # An equation.
wdGoToField = 7  # A field.
wdGoToFootnote = 4  # A footnote.
wdGoToGrammaticalError = 14  # A grammatical error.
wdGoToGraphic = 8  # A graphic.
wdGoToHeading = 11  # A heading.
wdGoToLine = 3  # A line.
wdGoToObject = 9  # An object.
wdGoToPage = 1  # A page.
wdGoToPercent = 12  # A percent.
wdGoToProofreadingError = 15  # A proofreading error.
wdGoToSection = 0  # A section.
wdGoToSpellingError = 13  # A spelling error.
wdGoToTable = 2  # A table.

# WdGoToDirection
wdGoToAbsolute = 1  # An absolute position.
wdGoToFirst = 1  # The first instance of the specified object.
wdGoToLast = -1  # The last instance of the specified object.
wdGoToNext = 2  # The next instance of the specified object.
wdGoToPrevious = 3  # The previous instance of the specified object.
wdGoToRelative = 2  # A position relative to the current position.


# WdBreakType
wdColumnBreak = 8  # Column break at the insertion point.
wdLineBreak = 6  # Line break.
wdLineBreakClearLeft = 9  # Line break.
wdLineBreakClearRight = 10  # Line break.
wdPageBreak = 7  # Page break at the insertion point.
wdSectionBreakContinuous = 3  # New section without a corresponding page break.
# Section break with the next section beginning on the next even-numbered page. If the section break falls on an even-numbered page, Word leaves the next odd-numbered page blank.
wdSectionBreakEvenPage = 4
wdSectionBreakNextPage = 2  # Section break on next page.
# Section break with the next section beginning on the next odd-numbered page. If the section break falls on an odd-numbered page, Word leaves the next even-numbered page blank.
wdSectionBreakOddPage = 5
# Ends the current line and forces the text to continue below a picture, table, or other item. The text continues on the next blank line that does not contain a table aligned with the left or right margin.
wdTextWrappingBreak = 11


# WdFindWrap
# After searching the selection or range, Microsoft Word displays a message asking whether to search the remainder of the document.
wdFindAsk = 2
# The find operation continues if the beginning or end of the search range is reached.
wdFindContinue = 1
# The find operation ends if the beginning or end of the search range is reached.
wdFindStop = 0

# WdReplace
wdReplaceAll = 2  # Replace all occurrences.
wdReplaceNone = 0  # Replace no occurrences.
wdReplaceOne = 1  # Replace the first occurrence encountered.


class WdSaveFormat:
    wdFormatDocument = 0  # Microsoft Office Word 97 - 2003 binary file format.
    wdFormatDOSText = 4  # Microsoft DOS text format.
    # Microsoft DOS text with line breaks preserved.
    wdFormatDOSTextLineBreaks = 5
    wdFormatEncodedText = 7  # Encoded text format.
    wdFormatFilteredHTML = 10  # Filtered HTML format.
    wdFormatFlatXML = 19  # Open XML file format saved as a single XML file.
    # Open XML file format with macros enabled saved as a single XML file.
    wdFormatFlatXMLMacroEnabled = 20
    # Open XML template format saved as a XML single file.
    wdFormatFlatXMLTemplate = 21
    # Open XML template format with macros enabled saved as a single XML file.
    wdFormatFlatXMLTemplateMacroEnabled = 22
    wdFormatOpenDocumentText = 23  # OpenDocument Text format.
    wdFormatHTML = 8  # Standard HTML format.
    wdFormatRTF = 6  # Rich text format (RTF).
    wdFormatStrictOpenXMLDocument = 24  # Strict Open XML document format.
    wdFormatTemplate = 1  # Word template format.
    wdFormatText = 2  # Microsoft Windows text format.
    # Windows text format with line breaks preserved.
    wdFormatTextLineBreaks = 3
    wdFormatUnicodeText = 7  # Unicode text format.
    wdFormatWebArchive = 9  # Web archive format.
    wdFormatXML = 11  # Extensible Markup Language (XML) format.
    wdFormatDocument97 = 0  # Microsoft Word 97 document format.
    # Word default document file format. For Word, this is the DOCX format.
    wdFormatDocumentDefault = 16
    wdFormatPDF = 17  # PDF format.
    wdFormatTemplate97 = 1  # Word 97 template format.
    wdFormatXMLDocument = 12  # XML document format.
    # XML document format with macros enabled.
    wdFormatXMLDocumentMacroEnabled = 13
    wdFormatXMLTemplate = 14  # XML template format.
    # XML template format with macros enabled.
    wdFormatXMLTemplateMacroEnabled = 15
    wdFormatXPS = 18  # XPS format.

    @classmethod
    def IsSaveFormat(cls, val):
        if val in [cls.wdFormatDocument, cls.wdFormatDOSText, cls.wdFormatDOSTextLineBreaks,
                   cls.wdFormatEncodedText, cls.wdFormatFilteredHTML, cls.wdFormatFlatXML,
                   cls.wdFormatFlatXMLMacroEnabled, cls.wdFormatFlatXMLTemplate,
                   cls.wdFormatFlatXMLTemplateMacroEnabled,
                   cls.wdFormatOpenDocumentText, cls.wdFormatHTML, cls.wdFormatRTF,
                   cls.wdFormatStrictOpenXMLDocument, cls.wdFormatTemplate, cls.wdFormatText,
                   cls.wdFormatTextLineBreaks, cls.wdFormatUnicodeText, cls.wdFormatWebArchive,
                   cls.wdFormatXML, cls.wdFormatDocument97, cls.wdFormatDocumentDefault,
                   cls.wdFormatPDF, cls.wdFormatTemplate97, cls.wdFormatXMLDocument,
                   cls.wdFormatXMLDocumentMacroEnabled, cls.wdFormatXMLTemplate, cls.wdFormatXMLTemplateMacroEnabled,
                   cls.wdFormatXPS]:
            return True
        return False


class WdColor:
    wdColorAqua = 13421619  # Aqua color.
    wdColorAutomatic = -16777216  # Automatic color. Default; usually black.
    wdColorBlack = 0  # Black color.
    wdColorBlue = 16711680  # Blue color.
    wdColorBlueGray = 10053222  # Blue-gray color.
    wdColorBrightGreen = 65280  # Bright green color.
    wdColorBrown = 13209  # Brown color.
    wdColorDarkBlue = 8388608  # Dark blue color.
    wdColorDarkGreen = 13056  # Dark green color.
    wdColorDarkRed = 128  # Dark red color.
    wdColorDarkTeal = 6697728  # Dark teal color.
    wdColorDarkYellow = 32896  # Dark yellow color.
    wdColorGold = 52479  # Gold color.
    wdColorGray05 = 15987699  # Shade 05 of gray color.
    wdColorGray10 = 15132390  # Shade 10 of gray color.
    wdColorGray125 = 14737632  # Shade 125 of gray color.
    wdColorGray15 = 14277081  # Shade 15 of gray color.
    wdColorGray20 = 13421772  # Shade 20 of gray color.
    wdColorGray25 = 12632256  # Shade 25 of gray color.
    wdColorGray30 = 11776947  # Shade 30 of gray color.
    wdColorGray35 = 10921638  # Shade 35 of gray color.
    wdColorGray375 = 10526880  # Shade 375 of gray color.
    wdColorGray40 = 10066329  # Shade 40 of gray color.
    wdColorGray45 = 9211020  # Shade 45 of gray color.
    wdColorGray50 = 8421504  # Shade 50 of gray color.
    wdColorGray55 = 7566195  # Shade 55 of gray color.
    wdColorGray60 = 6710886  # Shade 60 of gray color.
    wdColorGray625 = 6316128  # Shade 625 of gray color.
    wdColorGray65 = 5855577  # Shade 65 of gray color.
    wdColorGray70 = 5000268  # Shade 70 of gray color.
    wdColorGray75 = 4210752  # Shade 75 of gray color.
    wdColorGray80 = 3355443  # Shade 80 of gray color.
    wdColorGray85 = 2500134  # Shade 85 of gray color.
    wdColorGray875 = 2105376  # Shade 875 of gray color.
    wdColorGray90 = 1644825  # Shade 90 of gray color.
    wdColorGray95 = 789516  # Shade 95 of gray color.
    wdColorGreen = 32768  # Green color.
    wdColorIndigo = 10040115  # Indigo color.
    wdColorLavender = 16751052  # Lavender color.
    wdColorLightBlue = 16737843  # Light blue color.
    wdColorLightGreen = 13434828  # Light green color.
    wdColorLightOrange = 39423  # Light orange color.
    wdColorLightTurquoise = 16777164  # Light turquoise color.
    wdColorLightYellow = 10092543  # Light yellow color.
    wdColorLime = 52377  # Lime color.
    wdColorOliveGreen = 13107  # Olive green color.
    wdColorOrange = 26367  # Orange color.
    wdColorPaleBlue = 16764057  # Pale blue color.
    wdColorPink = 16711935  # Pink color.
    wdColorPlum = 6697881  # Plum color.
    wdColorRed = 255  # Red color.
    wdColorRose = 13408767  # Rose color.
    wdColorSeaGreen = 6723891  # Sea green color.
    wdColorSkyBlue = 16763904  # Sky blue color.
    wdColorTan = 10079487  # Tan color.
    wdColorTeal = 8421376  # Teal color.
    wdColorTurquoise = 16776960  # Turquoise color.
    wdColorViolet = 8388736  # Violet color.
    wdColorWhite = 16777215  # White color.
    wdColorYellow = 65535  # Yellow color.


class WdUnderline:
    wdUnderlineDash = 7  # Dashes.
    wdUnderlineDashHeavy = 23  # Heavy dashes.
    wdUnderlineDashLong = 39  # Long dashes.
    wdUnderlineDashLongHeavy = 55  # Long heavy dashes.
    wdUnderlineDotDash = 9  # Alternating dots and dashes.
    wdUnderlineDotDashHeavy = 25  # Alternating heavy dots and heavy dashes.
    wdUnderlineDotDotDash = 10  # An alternating dot-dot-dash pattern.
    # An alternating heavy dot-dot-dash pattern.
    wdUnderlineDotDotDashHeavy = 26
    wdUnderlineDotted = 4  # Dots.
    wdUnderlineDottedHeavy = 20  # Heavy dots.
    wdUnderlineDouble = 3  # A double line.
    wdUnderlineNone = 0  # No underline.
    wdUnderlineSingle = 1  # A single line. default.
    wdUnderlineThick = 6  # A single thick line.
    wdUnderlineWavy = 11  # A single wavy line.
    wdUnderlineWavyDouble = 43  # A double wavy line.
    wdUnderlineWavyHeavy = 27  # A heavy wavy line.
    wdUnderlineWords = 2  # Underline individual words only.


class WdParagraphAlignment:
    wdAlignParagraphCenter = 1  # Center-aligned.
    # Paragraph characters are distributed to fill the entire width of the paragraph.
    wdAlignParagraphDistribute = 4
    wdAlignParagraphJustify = 3  # Fully justified.
    # Justified with a high character compression ratio.
    wdAlignParagraphJustifyHi = 7
    # Justified with a low character compression ratio.
    wdAlignParagraphJustifyLow = 8
    # Justified with a medium character compression ratio.
    wdAlignParagraphJustifyMed = 5
    wdAlignParagraphLeft = 0  # Left-aligned.
    wdAlignParagraphRight = 2  # Right-aligned.
    # Justified according to Thai formatting layout.
    wdAlignParagraphThaiJustify = 9


class WdSaveOptions:
    wdDoNotSaveChanges = 0  # Do not save pending changes.
    wdPromptToSaveChanges = -2  # Prompt the user to save pending changes.
    # Save pending changes automatically without prompting the user.
    wdSaveChanges = -1

    @classmethod
    def IsSaveOptions(cls, val):
        if val == cls.wdDoNotSaveChanges or\
                val == cls.wdPromptToSaveChanges or\
                val == cls.wdSaveChanges:
            return True
        return False


class WdOriginalFormat:
    wdOriginalDocumentFormat = 1  # Original document format.
    wdPromptUser = 2  # Prompt user to select a document format.
    wdWordDocument = 0  # Microsoft Word document format.

    @classmethod
    def IsOriginalFormat(cls, val):
        if val == cls.wdOriginalDocumentFormat or\
                val == cls.wdPromptUser or\
                val == cls.wdWordDocument:
            return True
        return False


class WordEvents:
    def OnQuit(self):
        # outputdebug('word quit')
        # global oWord
        # oWord = None
        pass


# def outputdebug(msg):
#     win32api.OutputDebugString(msg)


# def AppCheck():
#     global oWord
#     try:
#         oWord.Visible
#         # outputdebug('read Visible successful')
#     except Exception:
#         # outputdebug('read Visible failed')
#         oWord = None


def VerCheck(ver):
    if ver.startswith('16.') or ver.startswith('15.') or ver.startswith('14.') or ver.startswith('12.'):
        global version
        version = ver
        return True
    return False


def DocCheck(doc):
    name = str(type(doc))
    pos = name.find('win32com.gen_py')
    if pos == -1:
        return False
    else:
        return True


def StrToInt(Obj, numbase=0):
    succ = False
    res = None
    try:
        if isinstance(Obj, int):
            res = Obj
            succ = True
        elif isinstance(Obj, str):
            res = int(Obj, numbase)
            succ = True
        else:
            raise Exception('颜色值错误')
    except Exception as e:
        res = e
    return (succ, res)


def open2(params):
    try:
        path = params['path']
        visitPwd = params['visitPwd']
        editPwd = params['editPwd']
        visiable = params['visiable']
        if not (path.endswith('.doc') or path.endswith('.docx')):
            raise Exception("不是Word文档")
        wd = openWord2(path, visitPwd, editPwd, visiable)
        params['word'] = wd['word']
        data = {
            "path": path,
            "visitPwd": visiable,
            "editPwd": editPwd,
            "visiable": visiable,
            "word": wd['word']
        }
        return data
    except Exception as e:
        raise e


def openWord2(path, visitPwd, editPwd, visiable):
    try:
        if visiable == 'yes':
            bVisible = True
        if visiable == "no":
            bVisible = False
        # AppCheck()
        # global oWord
        # if oWord is None:
        oWord = win32com.client.DispatchWithEvents(
            'Word.Application', WordEvents)
        ver = oWord.Version
        # outputdebug(ver)

        if VerCheck(ver):
            oWord.Visible = bVisible
            if bVisible:
                oWord.Activate()
            if not os.path.isfile(path):
                doc = oWord.Documents.Add('')
                doc.Activate()
                if path.endswith('.doc'):
                    doc.SaveAs(path, WdSaveFormat.wdFormatDocument,
                               False, visitPwd, True, editPwd)
                else:
                    doc.SaveAs(path, WdSaveFormat.wdFormatDocumentDefault, False, visitPwd, True,
                               editPwd)
                return {
                    'word': doc,
                    'app': oWord
                }
            else:
                for doc in oWord.Documents:
                    fullname = doc.FullName
                    if fullname.lower() == path.lower():
                        doc.Activate()
                        return {
                            'word': doc,
                            'app': oWord
                        }
                doc = oWord.Documents.Open(
                    path, True, False, True, visitPwd, '', False, editPwd, '', wdOpenFormatAuto)
                # setObj(oWord)
                doc.Activate()
                return {
                    'word': doc,
                    'app': oWord
                }
        else:
            raise Exception('不支持的Word版本')
    except Exception as e:
        raise e


def read(params):
    try:
        word = params['doc']['word']
        if not word:
            return ''

        if not DocCheck(word):
            raise Exception('must be com object')

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception as e:
            print(e)
        if win:
            win.Activate()
            return win.Selection.Text
        else:
            return ''
    except Exception as e:
        raise e


def readAll(params):
    try:
        word = params['doc']['word']
        if not word:
            return ''

        if not DocCheck(word):
            raise Exception('must be com object')

        return word.Content.Text

    except Exception as e:
        raise e


def reWrite(params):
    try:

        text = params["text"]
        word = params['doc']['word']
        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.WholeStory()
            win.Selection.Delete()
            if not text.endswith('\n'):
                text += '\n'
            win.Selection.Range.Text = text
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def setPosition(params):
    try:
        style = params['type']

        num = int(params['num'])
        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.SetRange(0, 0)

        if num:
            if style == 'char':
                win.Selection.Move(wdCharacter, num)
            elif style == 'line':
                win.Selection.Move(wdLine, num)
            elif style == 'section':
                win.Selection.Move(wdParagraph, num)

        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def setTextPosition(params):
    try:
        pos = params['position']
        text = params['text']
        # word = openWord(path, visitPwd, editPwd, visiable)
        word = params['doc']['word']
        word.Activate()
        win = None
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            word.Select()
            succ = win.Selection.Find.Execute(
                text, False, False, False, False, False, True, wdFindStop, False, "", wdReplaceNone)
            if succ:
                if pos == 'select':
                    pass
                elif pos == 'before':
                    win.Selection.Collapse(wdCollapseStart)
                elif pos == 'after':
                    win.Selection.Collapse(wdCollapseEnd)
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def movePosition(params):
    try:
        shift = params['shift']
        num = int(params["num"])
        style = params['style']
        direct = params['direction']
        cdirect = params['cdirection']
        # if num.isdigit():
        #     num = int(num)
        if num == 0:
            return
        # word = openWord(path, visitPwd, editPwd, visiable)
        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            unit = None
            if style == 'char':
                unit = wdCharacter
                if cdirect == 'up':
                    cdirect = 'left'
                elif cdirect == 'down':
                    cdirect = 'right'
                start = 0
                end = 0
                if cdirect == 'left':
                    # the selection is collapsed to the endpoint and moved to the left.
                    win.Selection.Collapse(wdCollapseStart)
                    end = win.Selection.Start
                    win.Selection.MoveLeft(unit, num)
                    start = win.Selection.Start
                elif cdirect == 'right':
                    # the selection is collapsed to the endpoint and moved to the right
                    win.Selection.Collapse(wdCollapseEnd)
                    start = win.Selection.End
                    win.Selection.MoveRight(unit, num)
                    end = win.Selection.End
                if shift == 'yes':
                    win.Selection.SetRange(start, end)
            elif style == 'line' or style == 'section':
                if style == 'line':
                    unit = wdLine
                else:
                    unit = wdParagraph
                if direct == 'left':
                    direct = 'up'
                elif direct == 'right':
                    direct = 'down'
                start = 0
                end = 0
                # if style == 'section':
                #     num = num + 1
                if direct == 'up':
                    # the selection is collapsed to the endpoint and moved up.
                    win.Selection.Collapse(wdCollapseStart)
                    end = win.Selection.Start
                    win.Selection.MoveUp(unit, num)
                    start = win.Selection.Start
                elif direct == 'down':
                    # the selection is collapsed to the endpoint and moved down.
                    win.Selection.Collapse(wdCollapseEnd)
                    start = win.Selection.End
                    win.Selection.MoveDown(unit, num)
                    end = win.Selection.End
                if shift == 'yes':
                    win.Selection.SetRange(start, end)
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def select(params):
    try:
        start = int(params['start'])
        end = int(params['end'])

        if start < 0 or end < 0:
            raise Exception("请输入大于0的起始或结束行数")

        if start > end:
            line = start
            start = end
            end = line

        # word = openWord(path, visitPwd, editPwd, visiable)
        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.GoTo(wdGoToLine, wdGoToFirst, start)
            start = win.Selection.Start
            win.Selection.GoTo(wdGoToLine, wdGoToFirst, end)
            win.Selection.MoveDown(wdLine, 1, 1)
            end = win.Selection.End
            win.Selection.SetRange(start, end)
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def selectAll(params):
    try:
        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.WholeStory()
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def cut(params):
    try:
        word = params['doc']['word']
        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.Cut()
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def copy(params):
    try:
        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.Copy()
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def paste(params):
    try:
        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.Paste()
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def delete(params):
    try:
        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.TypeBackspace()
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def enter(params):
    try:
        word = params['doc']['word']
        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.TypeParagraph()
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def page(params):
    try:
        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.InsertBreak(wdPageBreak)
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def picture(params):
    try:
        copyFile = params['copyFile']
        saveToDoc = params['saveToDoc']
        picture = params['path']

        if copyFile == 'no':
            copyFile = False
        elif copyFile == 'yes':
            copyFile = True

        if saveToDoc == 'no':
            saveToDoc = False
        elif saveToDoc == 'yes':
            saveToDoc = True

        if not (copyFile or saveToDoc):
            raise Exception('独立副本和是否保存不能同时为否')

        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            word.InlineShapes.AddPicture(
                picture, copyFile, saveToDoc, win.Selection.Range)
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def write(params):
    try:
        content = str(params['content'])

        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.Range.Text = content
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def replace(params):
    try:
        case = params['case']
        loop = params['loop']
        targetStr = params['target']
        replaceStr = params['replace']
        whole = params['whole']
        wildcard = params['wildcard']
        forward = params['forward']
        replaceType = params['replaceType']

        if case == 'no':
            case = False
        elif case == 'yes':
            case = True

        if whole == 'no':
            whole = False
        elif whole == 'yes':
            whole = True

        if wildcard == 'no':
            wildcard = False
        elif wildcard == 'yes':
            wildcard = True

        if forward == 'no':
            forward = False
        elif forward == 'yes':
            forward = True

        if loop == '1':
            loop = wdFindStop
        elif loop == '2':
            loop = wdFindContinue
        elif loop == '3':
            loop = wdFindAsk

        if replaceType == '1':
            replaceType = wdReplaceOne
        elif replaceType == '2':
            replaceType = wdReplaceNone
        elif replaceType == '3':
            replaceType = wdReplaceAll

        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            # if win.Selection.Start == win.Selection.End:
            #     params['word'] = word
            #     return params
            res = win.Selection.Find.Execute(
                targetStr, case, whole, wildcard, False, False, forward, loop, False, replaceStr, replaceType)
            win.Selection.Collapse(wdCollapseEnd)
            return res
        else:
            return False
        # params['word'] = word
        # return params
    except Exception as e:
        raise e


def setFont(params):
    try:
        font = params['font']

        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.Font.Name = font
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def setFontSize(params):
    try:
        size = float(params['size'])

        if size < 0:
            raise Exception("请输入正数")

        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.Font.Size = size
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except ValueError:
        raise Exception("请输入正数")
    except Exception as e:
        raise e


def setFontColor(params):
    try:
        color = params['color']

        color = color.lower()
        if color == 'blue':
            color = WdColor.wdColorBlue
        elif color == 'green':
            color = WdColor.wdColorGreen
        elif color == 'black':
            color = WdColor.wdColorBlack
        elif color == 'orange':
            color = WdColor.wdColorOrange
        elif color == 'pink':
            color = WdColor.wdColorPink
        elif color == 'red':
            color = WdColor.wdColorRed
        elif color == 'yellow':
            color = WdColor.wdColorYellow
        elif color == 'white':
            color = WdColor.wdColorWhite
        elif color == 'violet':
            color = WdColor.wdColorViolet
        else:
            if color.isdigit():
                color = int(color)
            # else:
            #     clist = list(color)
            #     clist.reverse()
            #     color = "".join(clist)

            succ, _Color = StrToInt(color)
            if not succ:
                succ, _Color = StrToInt(color, 16)
                if not succ:
                    raise Exception('颜色值错误')
            color = _Color

        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.Font.Color = color
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def setFontStyle(params):
    try:
        bold = params['bold']
        italic = params['italic']
        underline = params['underline']
        underlineStyle = int(params['underlineStyle'])

        if bold == 'no':
            bold = False
        elif bold == 'yes':
            bold = True

        if italic == 'no':
            italic = False
        elif italic == 'yes':
            italic = True

        if underline == "no":
            underline = False
        elif underline == 'yes':
            underline = True

        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.Font.Bold = bold
            win.Selection.Font.Italic = italic
            if underline:
                win.Selection.Font.Underline = underlineStyle
            else:
                win.Selection.Font.Underline = WdUnderline.wdUnderlineNone
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def setAlign(params):
    try:
        align = params['align']

        if align == 'left':
            align = WdParagraphAlignment.wdAlignParagraphLeft
        elif align == 'right':
            align = WdParagraphAlignment.wdAlignParagraphRight
        elif align == 'center':
            align = WdParagraphAlignment.wdAlignParagraphCenter
        elif align == 'sides':
            align = WdParagraphAlignment.wdAlignParagraphJustify
        elif align == 'distribute':
            align = WdParagraphAlignment.wdAlignParagraphDistribute

        word = params['doc']['word']

        win = None
        word.Activate()
        try:
            win = word.ActiveWindow
        except Exception:
            pass
        if win:
            win.Activate()
            win.Selection.ParagraphFormat.Alignment = align
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def save(params):
    try:
        word = params['doc']['word']

        word.Save()
        params['word'] = word
        params['doc']['word'] = word

        return params['doc']
    except Exception as e:
        raise e


def saveAS(params):
    try:
        savePath = params['savePath']
        fileName = params['fileName']
        fileFormat = int(params['format'])
        word = params['doc']['word']
        if fileName == '':
            import os
            *_, fileName = os.path.split(word.FullName)
            fileName = os.path.splitext(fileName)[0]

        # word = openWord(path, visitPwd, editPwd, visiable)

        word.SaveAs(savePath + "\\" + fileName, fileFormat)

        params['word'] = word
        params['doc']['word'] = word

        return params['doc']

    except Exception as e:
        raise e


def close(params):
    try:
        doc = params["doc"]
        path = doc['path']
        visitPwd = doc['visitPwd']
        editPwd = doc['editPwd']
        visiable = doc['visiable']
        isCloseProcess = params['process']

        SaveChanges = WdSaveOptions.wdSaveChanges
        OriginalFormat = WdOriginalFormat.wdWordDocument

        wd = openWord2(path, visitPwd, editPwd, visiable)
        # return type(wd)
        # word = params['doc']['word']

        wd['word'].Close(SaveChanges, OriginalFormat, False)
        if isCloseProcess == 'yes':
            wd['app'].Quit(SaveChanges, OriginalFormat, False)

    except Exception as e:
        raise e


def quit(params):
    try:
        doc = params['doc']
        path = doc['path']
        visitPwd = doc['visitPwd']
        editPwd = doc['editPwd']
        visiable = doc['visiable']

        SaveChanges = WdSaveOptions.wdSaveChanges
        OriginalFormat = WdOriginalFormat.wdWordDocument

        wd = openWord2(path, visitPwd, editPwd, visiable)
        # global oWord
        # oWord = win32com.client.DispatchWithEvents('Word.Application', WordEvents)
        SaveChanges = WdSaveOptions.wdSaveChanges
        OriginalFormat = WdOriginalFormat.wdWordDocument
        # word = params['doc']['word']
        wd['word'].Close(SaveChanges, OriginalFormat, False)
        wd['app'].Quit(SaveChanges, OriginalFormat, False)
        # oWord = None

    except Exception as e:
        raise e


def getPath(params):
    try:
        word = params['doc']['word']

        return word.FullName

    except Exception as e:
        raise e


def get_doc_handle(params):
    try:
        document = None
        if 'doc_path' not in params.keys() or params['doc_path'] is None or params['doc_path'] == '':
            raise Exception('缺少参数：文档路径')
        else:
            doc_path = params['doc_path']
            if not os.path.exists(doc_path):
                raise Exception('文档不存在')
            else:
                document = Document(doc_path)

        return document
    except Exception as e:
        raise e
