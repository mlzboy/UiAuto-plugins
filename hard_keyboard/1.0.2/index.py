# -*- coding:utf-8 -*-
"""
by suyj
"""

import ctypes
from ctypes import *  # 导入 ctypes 库中所有模块
import os
import time


class _HardwareKeyBoard:

    def __init__(self):
        _root_path = os.path.dirname(os.path.join(__file__))
        _dll_path = os.path.join(_root_path, 'msdk.dll')
        self.msdk_dll = ctypes.windll.LoadLibrary(_dll_path)
        self.hdl = self.msdk_dll.M_Open(1)

    def key_send(self, str_, interval=.3):
        self.msdk_dll.M_KeyPress(self.hdl, 225, 1)  # 先按一次shift键，激活键盘
        time.sleep(.6)
        # 改成逐个输入，每个输入之间间隔0.2秒，避免因为输入太快，导致输入位数错误的情况
        for s in list(str_):
            str_buffer = create_string_buffer(1)  # 定义一个可变字符串变量，长度为128
            str_buffer.raw = bytes(s, encoding='utf-8')
            str_buffer_p = pointer(str_buffer)
            self.msdk_dll.M_KeyInputString(self.hdl, str_buffer_p, len(s))
            time.sleep(interval)

    def backspace(self, str_):
        for _ in str_:
            time.sleep(.05)
            self.press_key_hardware(42)

    def enter_key(self):
        self.msdk_dll.M_KeyPress(self.hdl, 40, 1)

    def enter_caps(self):
        self.press_key_hardware(57)

    def press_key_hardware(self, key_code):
        self.msdk_dll.M_KeyPress(self.hdl, key_code, 1)

    def press_enter(self):
        self.press_key_hardware(40)

    def press_down(self):
        self.press_key_hardware(81)

    def press_up(self):
        self.press_key_hardware(82)

    def press_tab(self):
        self.press_key_hardware(43)

    def press_esc(self):
        self.press_key_hardware(41)

    def press_left(self):
        self.press_key_hardware(80)

    def press_right(self):
        self.press_key_hardware(79)

    def __del__(self):
        try:
            self.msdk_dll.M_Close(self.hdl)
        except OSError as os_err:
            if '0x0000002F' in str(os_err):
                print('键盘模拟器未插入')
            else:
                raise os_err


def press_down():
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.press_down()


def press_up():
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.press_up()


def press_tab():
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.press_tab()


def press_key_hardware(key_code):
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.press_key_hardware(key_code)


def send_keys(str_, interval=.2):
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.key_send(str_, interval)
    pass


def press_esc():
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.press_esc()


def press_left():
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.press_left()


def press_right():
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.press_right()


def backspace(str_):
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.backspace(str_)


def enter_caps():
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.enter_caps()


def press_enter():
    hard_keyboard = _HardwareKeyBoard()
    hard_keyboard.press_enter()


def enter_key():
    press_enter()


def user_input(params):
    if params['operation'] == 'send_keys':
        send_keys(params['input_text'], 0.2)
    elif params['operation'] == 'enter_key':
        enter_key()
    elif params['operation'] == 'press_esc':
        press_esc()
    elif params['operation'] == 'press_tab':
        press_tab()

