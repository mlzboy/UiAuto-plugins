import sys
import json
import base64
import uiautomation as uiauto
from browser import ResumeBrowser, generate_xpath
import traceback


def key_send(params):
    try:
        target_element = params['target_element']

        element = None
        xpath = None
        if params['element_type'] == 'Xpath':
            if 'browser_info' in params.keys() and params['browser_info'] is not None:
                driver = params['browser_info']
                element = driver.find_element_by_xpath(params['element_xpath'])
                if element is not None:
                    element.clear()
                    element.send_keys(params['content'])
            else:
                raise Exception("缺少参数：浏览器对象")
        elif 'html' in target_element.keys():
            if 'browser_info' in params.keys() and params['browser_info'] is not None:
                driver = params['browser_info']
                # driver = ResumeBrowser(browser_info['executor_url'], browser_info['session_id'])
                html = target_element['html']
                element = None
                print("html的元素信息：", html)
                xpath = generate_xpath(html=html)
                print(xpath)
                element = driver.find_element_by_xpath(xpath)
                if element is not None:
                    element.clear()
                    element.send_keys(params['content'])
            else:
                raise Exception("缺少参数：浏览器对象")
        elif 'wnd' in target_element.keys():
            wnd = target_element['wnd']
            if "control_type_name" not in wnd.keys() or wnd["control_type_name"] is None or wnd["control_type_name"] == "":
                raise Exception(message="客户端界面元素没有control_type_name")
            elif "name" not in wnd.keys() or wnd["name"] is None or wnd["name"] == "":
                raise Exception(message="客户端界面元素没有name属性")
            elif hasattr(uiauto, wnd["control_type_name"]) is False:
                raise Exception(message="uiautomation不支持目标元素类型")
            else:
                control = getattr(uiauto, wnd["control_type_name"])(Name=wnd['name'])
                if hasattr(control, "SendKeys"):
                    control.SendKeys(params['content'])
        else:
            raise Exception(message="尚未支持当前元素的操作")

        return None
    except Exception as e:
        with open('C:\\logs\\log.txt', "a") as log_file:
            log_file.write(traceback.format_exc())
        print(e)
        raise e
