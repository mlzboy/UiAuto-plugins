const _ = require('lodash');


exports.substring = (params) => {
  let str = params.string || ''; // 需要处理的字符串
  const hGap = params.headGap || 0; // 头部切除长度
  const tGap = params.tailGap || 0; // 尾部切除长度
  let tailIndex = str.length - tGap;

  console.log('--------------substring--------------')
  console.log(str);
  console.log(hGap);
  console.log(tailIndex);
  console.log(str.substring(hGap, tailIndex));
  return Promise.resolve(str.substring(hGap, tailIndex));
}

exports.TwoDimensionalArrayToObject = (params) => {
  let data = params.data;
  let key = params.key;
  let range = _.compact(params.range.split(","));
  let value = range.length == 2 ? _.slice(data, range[0], range[1]) : _.slice(data, range[0]);
  let obj = [];
  _.each(value, item => {
    obj.push(_.zipObject(key, item))
  })
  return Promise.resolve(obj);
}

exports._map = (params) => {
  if (typeof params.required_key === 'string') {
    return Promise.resolve(_.map(params.array, params.required_key))
  } else if (Array.isArray(params.required_key)) {
    return Promise.resolve(_.map(params.array, object => {
      let result_object = {};
      _.forIn(object, (val, key) => {
        if (params.required_key.includes(key)) {
          result_object[key] = val;
        }
      })
      return result_object;
    }))
  }
}

exports._flatten = (params) => {
  console.log(params);
  return Promise.resolve(_.flatten(params.array));
}

exports.evaluation = (params) => {
  let data = params.data;
  new Function(params.evaluation)();
  return Promise.resolve(data);
};

exports.filterData = (params) => {
  const result = _.filter(params.data, (data) => {
    return data[params.field_name] = params.field_value;
  });

  return Promise.resolve(result);
};

exports.replaceCharacter = (params) => {
  const result = params.data.replace(params.old_character, params.new_character);

  return Promise.resolve(result);
};

exports.delcareVariable = (params) => {
  return Promise.resolve(params.value);
};

exports.temporary = (params) => {
  let data = params.firstArr;
  _.each(data, item => {
    let user = _.find(params.secondArr, { name: item.initname })
    item.email = user ? user.email : ''
  })
  return Promise.resolve(data);
}
