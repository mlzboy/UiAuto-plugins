from bs4 import BeautifulSoup
import json


def dataExtractor(params):
    table = params['table']
    soup = BeautifulSoup(table, "html.parser")
    output = []
    row_ind = 0
    col_ind = 0
    ths = soup.find_all("th")
    for row in soup.find_all('tr'):

        smallest_row_span = 1

        for cell in row.children:
            if cell.name in ('td', 'th'):

                row_span = int(cell.get('rowspan')) if cell.get(
                    'rowspan') else 1

                smallest_row_span = min(smallest_row_span, row_span)

                col_span = int(cell.get('colspan')) if cell.get(
                    'colspan') else 1

                while True:
                    if check_cell_validity(row_ind, col_ind, output):
                        break
                    col_ind += 1

                try:
                    insert(row_ind, col_ind, row_span, col_span,
                           cell.get_text(), output)
                except UnicodeEncodeError:
                    raise Exception(
                        'Failed to decode text; you might want to specify kwargs transformer=unicode')

                col_ind += col_span

        row_ind += smallest_row_span
        col_ind = 0

    if len(ths) > 0 and len(output) > 0:
        data = list()
        for tds in output[1:]:
            for idd, td in enumerate(tds):
                tdDic = dict()
                tdDic[ths[idd].text] = td
                tdDic = json.dumps(tdDic, ensure_ascii=False)
                data.append(tdDic)
        output = data

    return output


def check_cell_validity(i, j, output):

    if i >= len(output):
        return True
    if j >= len(output[i]):
        return True
    if output[i][j] is None:
        return True
    return False


def insert(i, j, height, width, val, output):

    for ii in range(i, i+height):
        for jj in range(j, j+width):
            insert_cell(ii, jj, val, output)


def insert_cell(i, j, val, output):
    while i >= len(output):
        output.append([])
    while j >= len(output[i]):
        output[i].append(None)

    if output[i][j] is None:
        output[i][j] = val
