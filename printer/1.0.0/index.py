import win32api
import os


def print_file(params):
    try:
        current_dir = os.path.dirname(__file__)
        GHOSTSCRIPT_PATH = current_dir + '\\gs\\bin\\gswin64c.exe '
        GSPRINT_PATH = current_dir + "\\gsprint\\gsprint.exe"

        if 'printer_name' not in params.keys() or params['printer_name'] is None or params['printer_name'] == '':
            raise Exception('缺少参数：打印机名称')
        elif 'file_path' not in params.keys() or params['file_path'] is None or params['file_path'] == '':
            raise Exception('缺少参数：文件路径')
        else:
            options = ' -dSAFER -dBATCH'
            printer = ' -printer "' + params["printer_name"] + '" '
            printer_file = '"' + current_dir + '\\' + params["file_path"] + '" '
            
            if params['print_range'] == 'range':
                if 'start_page' not in params.keys() or params['start_page'] is None:
                    raise Exception('缺少参数：开始页码')
                elif 'end_page' not in params.keys() or params['end_page'] is None:
                    raise Exception('缺少参数：开始页码')
                elif params['start_page'] <= 0 or params['end_page'] <= 0:
                    raise Exception('开始页码和结束页码必须是大于零的整数')
                else:
                    options = options + ' -from ' + str(params['start_page']) + ' -to ' + str(params['end_page'])
            else:
                options = options + ' -all'

            if params['duplex_type'] == 'duplex_vertical':
                options = options + ' -duplex_vertical'
            elif params['duplex_type'] == 'duplex_horizontal':
                options = options + ' -duplex_horizontal'
            else:
                pass

            win32api.ShellExecute(0, 'open', GSPRINT_PATH, ' -ghostscript "' + GHOSTSCRIPT_PATH + '"' 
                + options + printer + printer_file, '.', 0)
    except Exception as e:
        raise e
        
