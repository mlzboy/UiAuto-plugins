import configparser


def readKey(params):
    ini = params['ini']
    section = params['section']
    key = params['key']
    default = params['default']
    conf = configparser.ConfigParser()
    try:
        conf.read(ini)
        result = conf.get(section, key)
    except KeyError:
        result = default
        raise Exception("小节名不正确")
    except configparser.NoOptionError:
        result = default
        raise Exception("键名不正确")
    except Exception as e:
        raise e
    return result


def writeKey(params):
    try:
        ini = params['ini']
        section = params['section']
        key = params['key']
        value = params['value']
        conf = configparser.ConfigParser()
        conf.read(ini)
        sList = listSections(params)
        if section not in sList:
            conf.add_section(section)
        conf.set(section, key, value)

        conf.write(open(ini, "w"))
    except configparser.DuplicateSectionError:
        raise Exception("小节" + section + "已存在")
    except Exception as e:
        raise e


def listSections(params):
    try:
        ini = params["ini"]
        conf = configparser.ConfigParser()
        conf.read(ini)
        sections = conf.sections()
        return sections
    except Exception as e:
        raise e


def getKeys(params):
    try:
        ini = params['ini']
        section = params['section']
        conf = configparser.ConfigParser()
        conf.read(ini)
        if conf.has_section(section):
            keys = conf.options(section)
            return keys
        else:
            return "没有该小节"
    except Exception as e:
        raise e


def delSection(params):
    try:
        ini = params['ini']
        section = params['section']
        conf = configparser.ConfigParser()
        conf.read(ini)
        if conf.has_section(section):
            conf.remove_section(section)
            conf.write(open(ini, "w"))
            # return "删除成功"
        else:
            raise Exception("该配置文件没有该小节")
    except Exception as e:
        raise e


def delKey(params):
    try:
        ini = params['ini']
        section = params['section']
        key = params['key']
        conf = configparser.ConfigParser()
        conf.read(ini)
        if conf.has_section(section):
            if conf.has_option(section, key):
                conf.remove_option(section, key)
                conf.write(open(ini, "w"))
                # return "删除成功"
            else:
                raise Exception("该小节没有该键")
        else:
            raise Exception("该配置文件没有该小节")
    except Exception as e:
        raise e

